<?php

include '../header.php';

session_start();
include "../connect_db.php";

if(ISSET($_SESSION['level']) && $_SESSION['level']==1){
	include '../menu.php';
	?>
	<div class="container for-fixed-nav">
		<div class="row">
			<div class="col-sm-12">
				<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Welcome <b><?php echo $_SESSION['nama_user'];?> </b>
				</div>				
        		<?php if(ISSET($_SESSION['message'])){echo $_SESSION['message']; unset($_SESSION['message']);} ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Latest Contactor Progress</h3></div>
					<div class="panel-body">
					<?php
					$counter=1;
					$query=mysql_query("SELECT * FROM progres LEFT JOIN kontrak on progres.id_kontrak=kontrak.id_kontrak LEFT JOIN bulan ON progres.progres_bulan=bulan.id_bulan LEFT JOIN user ON progres.id_user=user.id_user ORDER BY progres.id_progres DESC");
					while(($row=mysql_fetch_array($query)) && ($counter<=10)){
					?>
						<div class="log-container col-sm-12">
							<span class="text-info">[<?php echo $row['tgl_submit']; ?>]</span> <br/>
							<b><?php echo $row['nama_user']; ?></b> add a new progress for project <a href="kontrak_detail.php?id_kontrak=<?php echo $row['id_kontrak']; ?>"><b><?php echo $row['judul_kontrak']; ?></b></a> period <?php echo $row['nama_bulan'].','.$row['progres_tahun']; ?> 
						</div>
					<?php
						$counter++;
					}
					?>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Project Statistic</h3></div>
					<div class="panel-body">
						<div class="box-body chart-responsive">
		                  <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
		                </div><!-- /.box-body -->
		                <?php
		                	$query_all=mysql_query("SELECT id_kontrak FROM kontrak");
		                	$query_finished=mysql_query("SELECT tgl_selesai FROM kontrak WHERE tgl_selesai<curdate()");
		                	$all=mysql_num_rows($query_all);
		                	$finished=mysql_num_rows($query_finished);
		                	//echo $finished;
		                ?>
					</div>
				</div>
		</div>
	</div>

<?php
} else{
	include '../error_handler.php';
	echo '<META HTTP-EQUIV="Refresh" CONTENT="5; URL=../index.php">';
}

include '../footer.php';

?>