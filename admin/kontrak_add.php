<?php

include '../header.php';

session_start();
include "../connect_db.php";

if(ISSET($_SESSION['level']) && $_SESSION['level']==1){
	include '../menu.php';
	?>
	<div class="container for-fixed-nav">

		<div class="row">
			<div class="col-sm-12">      
        <?php if(ISSET($_SESSION['message'])){echo $_SESSION['message']; unset($_SESSION['message']);} ?>
				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Add Project</h3></div>
					<div class="panel-body">

<form class="form-horizontal" action="kontrak_add_process.php" method="post">
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">No. Kontrak</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="no_kontrak">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">No. Masa Berlaku Kontrak</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="no_masa_berlaku">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Kontraktor</label>
    <div class="col-sm-10">
      	<select class="selectpicker" data-show-subtext="true" data-live-search="true" name="id_kontraktor">      	
      		<option></option>
	      	<?php
	      	$getContractor=mysql_query("SELECT id_user,nama_user FROM user WHERE level=2 ORDER BY nama_user ASC");
	      	while ($contractor=mysql_fetch_array($getContractor)) {
	      		?>
	      		<option value="<?php echo $contractor['id_user']; ?>"><?php echo $contractor['nama_user']; ?></option>
	      		<?php
	      	}
	      	?>
	    </select>
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Judul Kontrak</label>
    <div class="col-sm-10">
      <textarea type="text" class="form-control input-sm" id="" placeholder="" name="judul_kontrak"></textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Uraian Kontrak / Amandemen</label>
    <div class="col-sm-10">
      <textarea type="text" class="form-control input-sm" id="" placeholder="" name="uraian_kontrak"></textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Uraian Pekerjaan</label>
    <div class="col-sm-10">
      <textarea type="text" class="form-control input-sm" id="" placeholder="" name="uraian_pekerjaan"></textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Tanggal Mulai</label>
    <div class="col-sm-10 input-group custom date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="tgl_mulai">
      <div class="input-group-addon">
        <span class="glyphicon glyphicon-glyphicon glyphicon-calendar"></span>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Tanggal Selesai</label>
    <div class="col-sm-10 input-group custom date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="tgl_selesai">
      <div class="input-group-addon">
        <span class="glyphicon glyphicon-glyphicon glyphicon-calendar"></span>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Lokasi Pekerjaan</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="lokasi_pekerjaan">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Schedule</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="schedule">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">PIC</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="pic">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">No. HP/Telp</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="no_hp">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Pengawas Lapangan</label>
    <div class="col-sm-10">
        <select class="selectpicker" data-show-subtext="true" data-live-search="true" name="id_pengawas">       
          <option></option>
          <?php
          $getSupervisor=mysql_query("SELECT id_user,nama_user FROM user WHERE level=3 ORDER BY nama_user ASC");
          while ($supervisor=mysql_fetch_array($getSupervisor)) {
            ?>
            <option value="<?php echo $supervisor['id_user']; ?>"><?php echo $supervisor['nama_user']; ?></option>
            <?php
          }
          ?>
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="text" class="btn btn-success btn-sm" type="submit"><span class="glyphicon glyphicon-plus"></span> &nbsp;Add</button>
    </div>
  </div>
</form>
					</div>
				</div>
			</div>
		</div>

	</div>

<?php
} else{
	include '../error_handler.php';
	echo '<META HTTP-EQUIV="Refresh" CONTENT="5; URL=../index.php">';
}

include '../footer.php';

?>