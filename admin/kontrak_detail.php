<?php

include '../header.php';

session_start();
include "../connect_db.php";

if(ISSET($_SESSION['level']) && $_SESSION['level']==1){
	include '../menu.php';
	?>
	<div class="container for-fixed-nav">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<?php
						$id_kontrak=$_GET['id_kontrak'];
						$query=mysql_query("SELECT *,kontraktor.nama_user as nama_kontraktor,pengawas.nama_user as nama_pengawas FROM kontrak LEFT JOIN (SELECT id_user,nama_user FROM user) AS kontraktor ON kontrak.id_kontraktor=kontraktor.id_user LEFT JOIN (SELECT id_user,nama_user FROM user) AS pengawas ON kontrak.id_pengawas=pengawas.id_user WHERE id_kontrak='$id_kontrak'");
				    	while($row=mysql_fetch_array($query)){
					?>
					<div class="panel-heading"><h3 class="panel-title">Project '<?php echo $row['judul_kontrak'];?>'</h3></div>
					<div class="panel-body">
						<div class="col-sm-12">
							<hr/>
							<b>OVERVIEW</b> 
							<span class="pull-right">
							<a href="kontrak_edit.php?id_kontrak=<?php echo $row['id_kontrak']; ?>" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-edit"></span> &nbsp;Edit This Project</a> 
							<a href="../print_kontrak.php?id_kontrak=<?php echo $row['id_kontrak']; ?>" target="_blank" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-print" target="_blank"></span> &nbsp;Print</a>
							</span>
							<hr/>
						</div>
						<div class="row">
							<div class="col-sm-12">
							<label for="" class="col-sm-2 control-label">No. Kontrak</label>
							<div class="col-sm-10"><?php echo $row['no_kontrak']; ?></div>
						    </div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						    <label for="" class="col-sm-2 control-label">Kontraktor</label>
						    <div class="col-sm-10"><?php echo $row['nama_kontraktor']; ?></div>
						    </div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						    <label for="" class="col-sm-2 control-label">No. Masa Berlaku Kontrak</label>
						    <div class="col-sm-10"><?php echo $row['no_masa_berlaku']; ?></div>
						    </div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						    <label for="" class="col-sm-2 control-label">Judul Kontrak</label>
						    <div class="col-sm-10"><?php echo $row['judul_kontrak']; ?></div>
						    </div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						    <label for="" class="col-sm-2 control-label">Uraian Kotrak / Amandemen</label>
						    <div class="col-sm-10"><?php echo $row['uraian_kontrak']; ?></div>
						    </div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						    <label for="" class="col-sm-2 control-label">Uraian Pekerjaan</label>
						    <div class="col-sm-10"><?php echo $row['uraian_pekerjaan']; ?></div>
						    </div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						    <label for="" class="col-sm-2 control-label">Tanggal Mulai</label>
						    <div class="col-sm-10"><?php echo $row['tgl_mulai']; ?></div>
						    </div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						    <label for="" class="col-sm-2 control-label">Tanggal Selesai</label>
						    <div class="col-sm-10"><?php echo $row['tgl_selesai']; ?></div>
						    </div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						    <label for="" class="col-sm-2 control-label">Lokasi Pekerjaan</label>
						    <div class="col-sm-10"><?php echo $row['lokasi_pekerjaan']; ?></div>
						    </div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						    <label for="" class="col-sm-2 control-label">Schedule</label>
						    <div class="col-sm-10"><?php echo $row['schedule']; ?></div>
						    </div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						    <label for="" class="col-sm-2 control-label">PIC</label>
						    <div class="col-sm-10"><?php echo $row['pic']; ?></div>
						    </div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						    <label for="" class="col-sm-2 control-label">No. HP/Telp.</label>
						    <div class="col-sm-10"><?php echo $row['no_hp']; ?></div>
						    </div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						    <label for="" class="col-sm-2 control-label">Pengawas Lapangan</label>
						    <div class="col-sm-10"><?php echo $row['nama_pengawas']; ?></div>
						    </div>
						</div>
						<?php
							}
						?>
						<div class="col-sm-12">
							<hr/>
							<b>PROGRES REPORT</b> 
							<hr/>
							<table class="table table-bordered">
								<thead>
									<tr>
									<th>Periode</th>
									<th>Progres Fisik</th>
									<th>Kendala</th>
									<th>Foto</th>
									<th>Keterangan Foto</th>
									<th width="140">Kurva S</th>
									<th>Verified Status</th>
									<th>Mailed Status</th>
									<th>Keterangan Tambahan</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$getProgres=mysql_query("SELECT * FROM progres LEFT JOIN bulan ON  progres.progres_bulan=bulan.id_bulan WHERE id_kontrak='$id_kontrak' AND progres.verified_status=1 ORDER BY id_progres DESC");
								while($progres=mysql_fetch_array($getProgres)){
								?>
								<tr>
									<td><?php echo $progres['nama_bulan']." ".$progres['progres_tahun']; ?></td>
									<td><?php echo $progres['progres_fisik']; ?></td>
									<td><?php echo $progres['kendala']; ?></td>
									<td>
									<a href="../upload/photos/<?php echo $progres['foto']; ?>" download><img src="../upload/photos/<?php echo $progres['foto']; ?>" class="img-responsive" width="130" /></a><br/>
									<?php if(!empty($progres['foto2'])){ ?><a href="../upload/photos/<?php echo $progres['foto2']; ?>" download><img src="../upload/photos/<?php echo $progres['foto2']; ?>" class="img-responsive" width="130" /></a><br/><?php } ?>
					    			<?php if(!empty($progres['foto3'])){ ?><a href="../upload/photos/<?php echo $progres['foto3']; ?>" download><img src="../upload/photos/<?php echo $progres['foto3']; ?>" class="img-responsive" width="130" /></a><?php } ?>
									</td>
									<td><?php echo $progres['ket_foto']; ?></td>
					    			<td>
					    			<a href="../upload/documents/<?php echo $progres['kurva_s']; ?>" download><?php echo $progres['kurva_s']; ?></a>
					    			</td>
					    			<td class="text-center"><?php if($progres['verified_status']==1){ echo "<span class='glyphicon text-success glyphicon-ok'></span>";}else{echo "<span class='glyphicon text-danger glyphicon-remove'></span>";} ?></td>
					    			<td class="text-center"><?php if($progres['mailed_status']==1){ echo "<span class='glyphicon text-info glyphicon-ok'></span>";}else{echo "<span class='glyphicon text-danger glyphicon-remove'></span>";} ?></td>
					    			<td>Submited on <?php echo date('d-M-Y G:i',strtotime($progres['tgl_submit'])); ?> <br/><br/>Verified on <?php echo date('d-M-Y G:i',strtotime($progres['tgl_verifikasi'])); ?></td>
								</tr>
								<?php
								}
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
} else{
	include '../error_handler.php';
	echo '<META HTTP-EQUIV="Refresh" CONTENT="5; URL=../index.php">';
}

include '../footer.php';

?>