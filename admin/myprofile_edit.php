<?php

include '../header.php';

session_start();
include "../connect_db.php";

if(ISSET($_SESSION['level']) && $_SESSION['level']==1){
	include '../menu.php';
  $id_user=$_SESSION['id_user'];
  $query=mysql_query("SELECT * FROM user WHERE id_user='$id_user'");
	?>
	<div class="container for-fixed-nav">

		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Edit My Profile</h3></div>
					<div class="panel-body">

<form class="form-horizontal" action="myprofile_edit_process.php" method="post">
  <?php
    while($row=mysql_fetch_array($query)){
  ?>
  <input type="hidden" name="id_user" value="<?php echo $row['id_user']; ?>">
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Nama Kontraktor</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" value="<?php echo $row['nama_user']; ?>" name="nama_user">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Alamat</label>
    <div class="col-sm-10">
      <textarea type="text" class="form-control input-sm" name="alamat_user"><?php echo $row['alamat_user']; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">E-mail</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" value="<?php echo $row['email_user']; ?>" name="email_user">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">No. Telepon</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" value="<?php echo $row['no_telp']; ?>" name="no_telp">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" name="username" value="<?php echo $row['username']; ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="text" class="btn btn-success btn-sm" type="submit"><span class=" glyphicon glyphicon-floppy-disk"></span> &nbsp;Save Changes</button>
    </div>
  </div>
  <?php
  }
  ?>
</form>
					</div>
				</div>
			</div>
		</div>

	</div>

<?php
} else{
	include '../error_handler.php';
	echo '<META HTTP-EQUIV="Refresh" CONTENT="5; URL=../index.php">';
}

include '../footer.php';

?>