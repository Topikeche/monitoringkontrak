<?php

include '../header.php';

session_start();
include "../connect_db.php";

if(ISSET($_SESSION['level']) && $_SESSION['level']==1){
	include '../menu.php';
  $id_user=$_SESSION['id_user'];
  $query=mysql_query("SELECT * FROM user WHERE id_user='$id_user'");
	?>
	<div class="container for-fixed-nav">

		<div class="row">
			<div class="col-sm-12">
        <?php if(ISSET($_SESSION['message'])){echo $_SESSION['message']; unset($_SESSION['message']);} ?>
				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Change Password</h3></div>
					<div class="panel-body">

<form class="form-horizontal" action="password_process.php" method="post">
  <?php
    while($row=mysql_fetch_array($query)){
  ?>
  <input type="hidden" name="id_user" value="<?php echo $_SESSION['id_user']; ?>">
  <input type="hidden" name="password" value="<?php echo $row['password']; ?>">
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Old Password</label>
    <div class="col-sm-10">
      <input data-toggle="password" data-placement="before" class="form-control input-sm" type="password" placeholder="type here" name="old_password">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">New Password</label>
    <div class="col-sm-10">
      <input data-toggle="password" data-placement="before" class="form-control input-sm" type="password" placeholder="type here" name="new_password">
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="text" class="btn btn-success btn-sm" type="submit"><span class=" glyphicon glyphicon-floppy-disk"></span> &nbsp;Save Changes</button>
    </div>
  </div>
  <?php
  }
  ?>
</form>
					</div>
				</div>
			</div>
		</div>

	</div>

<?php
} else{
	include '../error_handler.php';
	echo '<META HTTP-EQUIV="Refresh" CONTENT="5; URL=index.php">';
}

include '../footer.php';

?>