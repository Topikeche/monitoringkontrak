<?php

include '../header.php';

session_start();
include "../connect_db.php";

if(ISSET($_SESSION['level']) && $_SESSION['level']==1){
	include '../menu.php';
	?>
	<div class="container for-fixed-nav">

		<div class="row">
			<div class="col-sm-12">
      
        <?php if(ISSET($_SESSION['message'])){echo $_SESSION['message']; unset($_SESSION['message']);} ?>
				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Add User</h3></div>
					<div class="panel-body">

<form class="form-horizontal" action="user_add_process.php" method="post">
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Nama</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="nama_user">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Alamat</label>
    <div class="col-sm-10">
      <textarea type="text" class="form-control input-sm" id="" placeholder="" name="alamat_user"></textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">E-mail</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="email_user">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">No. Telepon</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="no_telp">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="username">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
      <input type="text" class="form-control input-sm" id="" placeholder="" name="password" value="123456">
    </div>
  </div>
  <div class="form-group">
    <label for="" class="col-sm-2 control-label">Role</label>
    <div class="col-sm-10">
      <select class="selectpicker" data-show-subtext="true" data-live-search="true" name="level">
        <option value="1">Administrator</option>
        <option value="2" selected>Kontraktor</option>
        <option value="3">Pengawas Lapangan</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="text" class="btn btn-success btn-sm" type="submit"><span class="glyphicon glyphicon-plus"></span> &nbsp;Add Contractor</button>
    </div>
  </div>
</form>
					</div>
				</div>
			</div>
		</div>

	</div>

<?php
} else{
	include '../error_handler.php';
	echo '<META HTTP-EQUIV="Refresh" CONTENT="5; URL=../index.php">';
}

include '../footer.php';

?>