<?php

include '../header.php';

session_start();
include "../connect_db.php";

if(ISSET($_SESSION['level']) && $_SESSION['level']==1){
	include '../menu.php';
	?>
	<div class="container for-fixed-nav">

		<div class="row">
			<div class="col-sm-12">

        		<?php if(ISSET($_SESSION['message'])){echo $_SESSION['message']; unset($_SESSION['message']);} ?>
				<div class="panel panel-default">
					<div class="panel-heading">
					    <span class="panel-title">All User List</span>
					    <a href="user_add.php" class="pull-right"><span class="glyphicon glyphicon-plus"></span> Add New User</a>
					</div>
					<div class="panel-body">
					<table id="example1" class="table table-hover table-bordered">
				    	<thead>
				    	<tr>
				    		<th class="text-center">No.</th>
				    		<th class="text-center">Nama</th>
				    		<th class="text-center">Alamat</th>
				    		<th class="text-center">E-mail</th>
				    		<th class="text-center">No. Telepon</th>
				    		<th class="text-center">Role</th>
				    		<th class="text-center"></th>
				    	</tr>
				    	</thead>
				    	<tbody>
				    	<?php 
					        $numbering=1;
					        $query=mysql_query("SELECT * FROM user");
					        $jumlah=mysql_num_rows($query);
					        if ($jumlah!=0) {			        	
			                	while($row=mysql_fetch_array($query)){
						    	?>
							    <tr>
							    	<td><?php echo $numbering; ?></td>
							    	<td><?php echo $row['nama_user']; ?></td>
							    	<td><?php echo $row['alamat_user']; ?></td>
							    	<td><?php echo $row['email_user']; ?></td>
							    	<td><?php echo $row['no_telp']; ?></td>
							    	<td><?php echo $row['level']; ?></td>
							    	<td><a href="user_edit.php?id_user=<?php echo $row['id_user']; ?>" class="btn btn-xs btn-success">Edit</a></td>
							    </tr>
						        <?php
						        	$numbering++;
						    	} 
					        } else{
					        	echo "<div class='alert alert-warning'>No data available.</div>";
					        }
					        ?>
				    	</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>

	</div>

<?php
} else{
	include '../error_handler.php';
	echo '<META HTTP-EQUIV="Refresh" CONTENT="5; URL=../index.php">';
}

include '../footer.php';

?>