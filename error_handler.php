  <div class="container you-shall-not-pass">
    <div class="gandalf">
      <img src="../images/youshallnotpass.png" width="250" alt="Forbiden 403" class="img-responsive center-block" />
    </div>
  	<div class="alert alert-warning" role="alert">
		<b>WARNING!</b> Forbidden Error 403.<br/>
		<span class="colored-line yellow"></span>
		<br/><br/>
		You are not allowed to access this page. If you don't have any account, please register by contacting the adminisrator. If you have account, please <a href="../index.php" class="alert-link"><b>login</b></a> by using account that ave been created fot you.
	  </div>
  </div>