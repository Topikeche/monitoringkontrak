
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../js/jquery.min.js"></script> 
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>    
    <script src="../js/bootstrap-datepicker.min.js"></script>
    <script src="../js/bootstrap-select.min.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script src="../js/dataTables.bootstrap.min.js"></script>
    <!--<script src="../js/jquery.min.js"></script>-->
    <script src="../js/bootstrap-show-password.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="../js/morris.min.js"></script>
    <script>
      $(function () {
        $("#example1").DataTable();
      });
    </script>
    <script>
        $(function() {
            $('#password').password().on('show.bs.password', function(e) {
                $('#eventLog').text('On show event');
                $('#methods').prop('checked', true);
            }).on('hide.bs.password', function(e) {
                        $('#eventLog').text('On hide event');
                        $('#methods').prop('checked', false);
                    });
            $('#methods').click(function() {
                $('#password').password('toggle');
            });
        });
    </script>

    <script>
      $(function () {
        //"use strict";
        //DONUT CHART
        var donut = new Morris.Donut({
          element: 'sales-chart',
          resize: true,
          colors: ["#3c8dbc", "#00a65a"],
          data: [
            {label: "Finished Project", value: <?php echo $finished; ?>},
            {label: "On-Going Project", value: <?php echo $all-$finished; ?>}
          ],
          hideHover: 'auto'
        });
      });
    </script>

        <!-- JavaScript Includes -->
    <script src="assets/js/jquery.knob.js"></script>
    <!-- jQuery File Upload Dependencies -->
    <script src="assets/js/jquery.ui.widget.js"></script>
    <script src="assets/js/jquery.fileupload.js"></script>    
    <!-- Our main JS file -->
    <script src="assets/js/script.js"></script>
  </body>
</html>