<?php

session_start();
include "connect_db.php";
if(ISSET($_SESSION['level'])){
    if ($_SESSION['level']==1) {
        header("location:admin/");
    }else if($_SESSION['level']==2){
        header("location:kontraktor/");
    }else if($_SESSION['level']==3){
        header("location:pengawas/");
    }
}else{
  
?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>STO Login</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <div class="col-sm-4 col-sm-offset-4">
      <div class="form-login-wrapper">
        <div class="form-header"><p class="text-center">Please <b>LOGIN</b></p></div>
      <?php if(ISSET($_SESSION['message'])){echo $_SESSION['message']; unset($_SESSION['message']);} ?>
        <div class="form-input">
          <div class="form-group-sm">
            <form action="login_process.php" method="post">       
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user"></span></span>
                <input type="text" class="form-control" name="username" placeholder="Username">
              </div>
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-lock"></span></span>
                <input type="password" class="form-control" name="password" placeholder="Password" required=""/> 
              </div>
              <button class="btn btn-sm btn-success btn-block" type="submit">Login</button>   
            </form>
          </div>
        </div>
        <div class="form-footer">
          <p class="text-center"><i>Copyright © 2016. All other copyrights and trademarks here in are the property of their respective owners.</i></p>
        </div>
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

<?php
}
?>