<?php

include '../header.php';

session_start();
include "../connect_db.php";

if(ISSET($_SESSION['level']) && $_SESSION['level']==2){
	include '../menu.php';
  $id_progres=$_GET['id_progres'];
  $query=mysql_query("SELECT * FROM progres LEFT JOIN user ON progres.id_user=user.id_user WHERE progres.id_progres='$id_progres'");
	?>
	<div class="container for-fixed-nav">

		<div class="row">
			<div class="col-sm-12">
        <?php if(ISSET($_SESSION['message'])){echo $_SESSION['message']; unset($_SESSION['message']);} ?>
				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Edit Progress</h3></div>
					<div class="panel-body">
            <form class="form-horizontal" action="progres_edit_process.php" method="post" enctype="multipart/form-data">
              <?php
                while($row=mysql_fetch_array($query)){
              ?>
              <hr/>
              <b>OVERVIEW</b>
              <hr/>
              <div class="form-group">
                <input type="hidden" name="id_progres" value="<?php echo $row['id_progres']; ?>">
                <label for="" class="col-sm-2 control-label">Judul Kontrak</label>
                <div class="col-sm-10">
                  <select class="selectpicker" data-show-subtext="true" data-live-search="true" name="id_kontrak">
                    <?php
                    $k = $_SESSION['id_user'];
                    $getContract=mysql_query("SELECT id_kontrak,judul_kontrak FROM kontrak WHERE id_kontraktor='$k'");
                    while ($contract=mysql_fetch_array($getContract)) {
                      ?>
                      <option value="<?php echo $contract['id_kontrak']; ?>" <?php if($contract['id_kontrak']==$row['id_kontrak']){echo "selected";}?> ><?php echo $contract['judul_kontrak']; ?></option>
                      <?php
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Progres Bulan / Tahun</label>
                <div class="col-sm-6">
                  <select class="selectpicker" data-show-subtext="true" data-live-search="true" name="progres_bulan">        
                  <?php
                  $getBulan=mysql_query("SELECT * FROM bulan");
                  while ($bulan=mysql_fetch_array($getBulan)) {
                  ?>
                    <option value="<?php echo $bulan['id_bulan'];?>" <?php if($bulan['id_bulan']==$row['progres_bulan']){echo 'selected';} ?>><?php echo $bulan['nama_bulan']; ?></option>
                  <?php } ?>
                  </select>
                </div>
                <div class="col-sm-4">
                <select class="selectpicker" data-show-subtext="true" data-live-search="true" name="progres_tahun">   
                  <?php
                  $getTahun=mysql_query("SELECT * FROM tahun");
                  while ($tahun=mysql_fetch_array($getTahun)) {
                  ?>
                    <option value="<?php echo $tahun['id_tahun']; ?>" <?php if($tahun['id_tahun']==$row['progres_tahun']){echo 'selected';} ?>><?php echo $tahun['id_tahun']; ?></option>
                  <?php } ?>
                  </select>
                </div>
              </div>
              <hr/>
              <b>PROGRES DAN KENDALA</b>
              <hr/>
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Progres Fisik</label>
                <div class="col-sm-10">
                  <textarea type="text" class="form-control input-sm" id="" placeholder="" name="progres_fisik"><?php echo $row['progres_fisik']; ?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Kendala</label>
                <div class="col-sm-10">
                  <textarea type="text" class="form-control input-sm" id="" placeholder="" name="kendala"><?php echo $row['kendala']; ?></textarea>
                </div>
              </div>             
              <hr/>
              <b>DOKUMENTASI PROGRES</b>
              <hr/>
              <!--<div class="form-group">
                <label for="" class="col-sm-2 control-label">Foto</label>
                <div class="col-sm-10 input-group custom">
                    <span class="input-group-btn">
                        <span class="btn btn-info btn-file btn-sm">
                            Browse file &nbsp;<span class=" glyphicon glyphicon-folder-open"></span><input type="file" multiple name='foto'>
                        </span>
                    </span>
                    <input type="text" class="form-control input-sm" readonly>
                </div>
              </div>-->
              <div class="form-group">
                <label class="col-sm-2 control-label">Foto-1</label>              
                <div class="col-sm-10">
                  <img src="../upload/photos/<?php echo $row['foto'];?>" width="150"> <i>[..old picture]</i><br/><br/>
                  <input type="file" name="foto">
                  <p class="help-block">Please input png, jpg, jpeg or gif only</p>
                  </div>
              </div>
              
              <div class="form-group">
                <label class="col-sm-2 control-label">Foto-2</label>              
                <div class="col-sm-10">
                  <?php if(!empty($row['foto2'])){ ?><img src="../upload/photos/<?php echo $row['foto2'];?>" width="150"> <i>[..old picture]</i><br/><br/><?php } ?>
                  <input type="file" name="foto2">
                  <p class="help-block">Please input png, jpg, jpeg or gif only</p>
                  </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Foto-3</label>              
                <div class="col-sm-10">
                  <?php if(!empty($row['foto3'])){ ?><img src="../upload/photos/<?php echo $row['foto3'];?>" width="150"> <i>[..old picture]</i><br/><br/><?php } ?>
                  <input type="file" name="foto3">
                  <p class="help-block">Please input png, jpg, jpeg or gif only</p>
                  </div>
              </div>             
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Keterangan Foto</label>
                <div class="col-sm-10">
                  <textarea type="text" class="form-control input-sm" id="" placeholder="" name="ket_foto"><?php echo $row['ket_foto']; ?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Kurva S</label>        
                <div class="col-sm-10">
                  <a href="../upload/photos/<?php echo $row['kurva_s']; ?>" download><?php echo $row['kurva_s']; ?> <i>[..old document]</i></a> <br/><br/>
                  <input type="file" name="kurva_s">
                  <p class="help-block">Please input doc, docx or pdf files only</p>
                  </div>
              </div>
              <?php
              }
              ?>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="text" class="btn btn-success btn-sm" type="submit"><span class="glyphicon glyphicon-plus"></span> &nbsp;Save Changes</button>
                </div>
              </div>
            </form>
					</div>
				</div>
			</div>
		</div>

	</div>

<?php
} else{
	include '../error_handler.php';
	echo '<META HTTP-EQUIV="Refresh" CONTENT="5; URL=../index.php">';
}

include '../footer.php';

?>