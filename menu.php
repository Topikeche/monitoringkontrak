  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">STO</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

        <?php if($_SESSION['level']==1){ ?>
        <!-- MENU ADMIN -->
        <ul class="nav navbar-nav">
          <li><a href="index.php">Home</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Project <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="kontrak_list.php">Project List</a></li>
              <li><a href="kontrak_add.php">Add Project</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Progress <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="progres_list.php">All Progress List</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">User <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="user_list.php">User List</a></li>
              <li><a href="user_add.php">Add User</a></li>
            </ul>
          </li>
        </ul>
        <?php }else if($_SESSION['level']==2){ ?>
        <!-- MENU KONTRAKTOR -->
        <ul class="nav navbar-nav">
          <li><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Progress <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="progres_list.php">Progress List</a></li>
            <li><a href="progres_add.php">Add Progress</a></li>
          </ul>
        </li>
        </ul>
        <?php }else if($_SESSION['level']==3){ ?>
        <!-- MENU PENGAWAS -->
        <ul class="nav navbar-nav">
          <li><a href="index.php">Home</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Progress <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="progres_list.php">All Progress List</a></li>
              <li><a href="progres_list_uncomplete.php">Uncomplete Progress Action</a></li>
            </ul>
          </li>
        </ul>
        <?php } ?>

        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['nama_user'];?> <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="myprofile_edit.php"><span class="glyphicon glyphicon-user"></span> &nbsp;Edit Profile</a></li>
              <li><a href="password.php"><span class="glyphicon glyphicon-lock"></span> &nbsp;Change Password</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="../logout_process.php"><span class="glyphicon glyphicon-off"></span> &nbsp;Logout</a></li>
            </ul>
          </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>