<?php

include '../header.php';

session_start();
include "../connect_db.php";

if(ISSET($_SESSION['level']) && $_SESSION['level']==3){
	include '../menu.php';
	?>
	<div class="container for-fixed-nav">
		<div class="row">
			<div class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  Welcome <b><?php echo $_SESSION['nama_user'];?> </b>
			</div>
			<?php if(ISSET($_SESSION['message'])){echo $_SESSION['message']; unset($_SESSION['message']);} ?>
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">My Project</h3>
			  </div>
			  <div class="panel-body">

			<div class="table-responsive">
		    <table id="example1" class="table table-hover table-bordered">
		    	<thead>
		    	<tr>
		    		<th class="text-center">No.</th>
		    		<th class="text-center">No. Kontrak</th>
		    		<th class="text-center">Judul Kontrak</th>
		    		<th class="text-center">Kontraktor</th>
		    		<th class="text-center">No. Masa Berlaku Kontrak</th>
		    		<th class="text-center">Uraian Pekerjaan</th>
		    		<th class="text-center">Uraian Kontrak / Amandemen</th>
		    		<th class="text-center">Range Durasi Tanggal</th>
		    		<th class="text-center">Lokasi Pekerjaan</th>
		    		<th class="text-center">Schedule</th>
		    		<th class="text-center">PIC</th>
		    		<th class="text-center">No. HP/Telp.</th>		    		
		    		<th class="text-center">Pengawas Lapangan</th>
		    	</tr>
		    	</thead>
			    	<?php 
			    	$id_pengawas=$_SESSION['id_user'];
			        $numbering=1;
			        $query=mysql_query("SELECT *, kontraktor.nama_user as nama_kontraktor, pengawas.nama_user as nama_pengawas FROM kontrak LEFT JOIN (SELECT id_user,nama_user FROM user) AS kontraktor ON kontrak.id_kontraktor=kontraktor.id_user LEFT JOIN (SELECT id_user,nama_user FROM user) AS pengawas ON kontrak.id_pengawas=pengawas.id_user WHERE pengawas.id_user='$id_pengawas'");
			        $jumlah=mysql_num_rows($query);
			        if ($jumlah!=0) {			        	
	                	while($row=mysql_fetch_array($query)){
				    	?>
					    <tr>
					    	<td><?php echo $numbering; ?></td>
					    	<td><?php echo $row['no_kontrak']; ?></td>
					    	<td><a href="kontrak_detail.php?id_kontrak=<?php echo $row['id_kontrak']; ?>"><?php echo $row['judul_kontrak']; ?></a></td>
					    	<td><?php echo $row['nama_kontraktor']; ?></td>
					    	<td><?php echo $row['no_masa_berlaku']; ?></td>
					    	<td><?php echo substr($row['uraian_pekerjaan'],0,80).' ...'; ?></td>
					    	<td><?php echo substr($row['uraian_kontrak'],0,80).' ...'; ?></td>
					    	<td><?php echo date('d-M-Y',strtotime($row['tgl_mulai'])); ?> sampai <?php echo date('d-M-Y',strtotime($row['tgl_selesai'])); ?></td>
					    	<td><?php echo $row['lokasi_pekerjaan']; ?></td>
					    	<td><?php echo $row['schedule']; ?></td>
					    	<td><?php echo $row['pic']; ?></td>
					    	<td><?php echo $row['no_hp']; ?></td>
					    	<td><?php echo $row['nama_pengawas']; ?></td>
					    </tr>
				        <?php
				        	$numbering++;
				    	} 
			        } else{
			        	echo "<div class='alert alert-warning'>No data available.</div>";
			        }
			        ?>
		    	</tbody>
				</table>
				</div>
			  </div>
			</div>
		</div>
	</div>

<?php
} else{
	include '../error_handler.php';
	echo '<META HTTP-EQUIV="Refresh" CONTENT="5; URL=../index.php">';
}

include '../footer.php';

?>