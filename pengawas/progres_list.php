<?php

include '../header.php';

session_start();
include "../connect_db.php";

if(ISSET($_SESSION['level']) && $_SESSION['level']==3){
	include '../menu.php';
	?>
	<div class="container for-fixed-nav">

		<div class="row">
			<div class="col-sm-12">
			<?php if(ISSET($_SESSION['message'])){echo $_SESSION['message']; unset($_SESSION['message']);} ?>
				<div class="panel panel-default">
					  <div class="panel-heading">
					    <span class="panel-title">All Progress List</span>					    
					  </div>
					  <div class="panel-body">
			
		    <table id="example1" class="table table-hover table-bordered">
		    	<thead>
		    	<tr>
		    		<th class="text-center">No.</th>
		    		<th class="text-center">Periode</th>
		    		<th class="text-center">Judul Kontrak</th>		    		
		    		<th class="text-center">Kontraktor</th>
		    		<th class="text-center">Progres Fisik</th>
		    		<th class="text-center">Kendala</th>
		    		<th class="text-center">Foto</th>
		    		<th class="text-center">Keterangan Foto</th>
		    		<th class="text-center">Kurva S</th>
		    		<th></th>
		    	</tr>
		    	</thead>
		    	<tbody>
		    	<?php
		    		$id_pengawas=$_SESSION['id_user'];
					$numbering=1;
			        $query=mysql_query("SELECT * FROM progres LEFT JOIN kontrak on progres.id_kontrak=kontrak.id_kontrak LEFT JOIN bulan ON progres.progres_bulan=bulan.id_bulan LEFT JOIN user ON progres.id_user=user.id_user WHERE kontrak.id_pengawas='$id_pengawas' ORDER BY progres.id_progres DESC");
			        $jumlah=mysql_num_rows($query);
			        if ($jumlah!=0) {			        	
	                	while($row=mysql_fetch_array($query)){
				    	?>
					    <tr class="<?php if($row['verified_status']==0){echo 'danger';}else if($row['mailed_status']==0){echo 'info';} ?>">
					    	<td><?php echo $numbering; ?></td>
					    	<td><?php echo $row['nama_bulan']." ".$row['progres_tahun']; ?></td>
					    	<td><?php echo $row['judul_kontrak']; ?></td>					    	
					    	<td><?php echo $row['nama_user']; ?></td>
					    	<td><?php echo $row['progres_fisik']; ?></td>
					    	<td width="300"><?php echo $row['kendala']; ?></td>
					    	<td>
					    	<a href="../upload/photos/<?php echo $row['foto']; ?>" download><img src="../upload/photos/<?php echo $row['foto']; ?>" class="img-responsive" width="130" /></a><br/>
					    	<?php if(!empty($row['foto2'])){ ?><a href="../upload/photos/<?php echo $row['foto2']; ?>" download><img src="../upload/photos/<?php echo $row['foto2']; ?>" class="img-responsive" width="130" /></a><br/><?php } ?>
					    	<?php if(!empty($row['foto3'])){ ?><a href="../upload/photos/<?php echo $row['foto3']; ?>" download><img src="../upload/photos/<?php echo $row['foto3']; ?>" class="img-responsive" width="130" /></a><?php } ?>
					    	</td>
					    	<td><?php echo $row['ket_foto']; ?></td>
					    	<td><a href="../upload/documents/<?php echo $row['kurva_s']; ?>" download><?php echo $row['kurva_s']; ?></a></td>
					    	<td>
					    	<a href="progres_verified.php?id_progres=<?php echo $row['id_progres']; ?>" onclick="return confirm('Are you sure to verified this progress?')" class="btn btn-xs btn-success col-sm-12 <?php if($row['verified_status']==1){echo 'disabled';} ?>" role="button">Verified This</a><br/><br/>
					    	<a href="progres_send.php?id_progres=<?php echo $row['id_progres']; ?>" onclick="return confirm('Are you sure to send this progress?')" class="btn btn-xs btn-info col-sm-12 <?php if($row['mailed_status']==1 || $row['verified_status']==0){echo 'disabled';} ?>" role="button">Send Mail</a>
					    	</td>
					    </tr>
				        <?php
				        	$numbering++;
				    	} 
			        } else{
			        	echo "<div class='alert alert-warning'>No data available.</div>";
			        }
			        ?>
		    	</tbody>
			</table>
			

					  </div>
					</div>
				</div>
		</div>

	</div>

<?php
} else{
	include '../error_handler.php';
	echo '<META HTTP-EQUIV="Refresh" CONTENT="5; URL=../index.php">';
}

include '../footer.php';

?>